import { CSSObject } from "@chakra-ui/styled-system";
import { extendTheme } from "@chakra-ui/react";

export type Styles = Record<string, CSSObject>;

const theme = extendTheme({
  colors: {
    primary: {
      50: "#ffe3e5",
      100: "#feb6b8",
      200: "#f7888a",
      300: "#f3595c",
      400: "#ee2b2f",
      500: "#d41115",
      600: "#a60a0f",
      700: "#77060a",
      800: "#4a0204",
      900: "#200000",
    },
    secondary: {
      50: "#e6f6f6",
      100: "#d2dcdc",
      200: "#bac2c2",
      300: "#a1aaaa",
      400: "#899292",
      500: "#6f7878",
      600: "#555e5e",
      700: "#3c4343",
      800: "#202929",
      900: "#001010",
    },
    // secondary: {
    //   50: "#fbf0f2",
    //   100: "#dcd8d9",
    //   200: "#bfbfbf",
    //   300: "#a6a6a6",
    //   400: "#8c8c8c",
    //   500: "#737373",
    //   600: "#595959",
    //   700: "#404040",
    //   800: "#282626",
    //   900: "#150a0d",
    // },
  },
  // text:
});

export default theme;
// #F5E0B7
// #11151C
// #333745

// #262626
// #DDF8E8
// // #white
// #F7F7FF

// #D6D2D2

// black
// 11151C

// gunmetal
// #333745
