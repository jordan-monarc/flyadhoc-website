import { Box, Flex, Heading, Stack, Text, VStack } from "@chakra-ui/layout";
import { Button, Link } from "@chakra-ui/react";
import React, { FC } from "react";
import Logo from "./Logo";
import { Styles } from "../styles/theme";

const styles: Styles = {
  wrapper: {
    w: "full",
    h: "100vh",
    backgroundImage:
      "url(https://images.unsplash.com/photo-1488085061387-422e29b40080?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=eva-darron-oCdVtGFeDC0-unsplash.jpg)",
    backgroundSize: "cover",
    backgroundPosition: "center center",
  },
  innerWrapper: {
    w: "full",
    justifyContent: "center",
    flexDir: "column",
    bgGradient: "linear(to-r, blackAlpha.600, transparent)",
  },
  logo: {
    pt: 6,
    pl: 6,
  },
  stackWrapper: {
    w: "full",
    justifyContent: "center",
    px: { base: 4, md: 8 },
    flex: 1,
  },
  stack: {
    maxW: "2xl",
    alignItems: "flex-start",
  },
  heading: {
    fontWeight: 600,
    fontSize: { base: "2xl", sm: "4xl", md: "6xl" },
    lineHeight: "110%",
  },
};
const Hero: FC = () => {
  return (
    <Flex sx={styles.wrapper}>
      <Flex sx={styles.innerWrapper}>
        <Box sx={styles.logo}>
          <Logo />
        </Box>

        <VStack sx={styles.stackWrapper}>
          <Stack sx={styles.stack} spacing={6}>
            <Heading sx={styles.heading}>
              {/* Fly Adhoc <br /> */}
              <Text as={"span"} color={"primary.400"}>
                {/* your audience */}
                Australia's First{" "}
              </Text>
              <Text as={"span"} color={"white"}>
                Real-Time Aircraft Charter Booking Service
              </Text>
            </Heading>

            <Stack direction={"row"}>
              <Button
                colorScheme={"primary"}
                rounded={"full"}
                as={Link}
                href={"#widget"}
              >
                Book Now
              </Button>
              <Button
                bg={"whiteAlpha.300"}
                rounded={"full"}
                color={"white"}
                _hover={{ bg: "whiteAlpha.500" }}
              >
                Show me more
              </Button>
            </Stack>
          </Stack>
        </VStack>
      </Flex>
    </Flex>
  );
};

export default Hero;
