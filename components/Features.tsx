import { Center, Heading } from "@chakra-ui/layout";
import { Box, Flex, Icon, SimpleGrid, Stack, Text } from "@chakra-ui/react";
import { ReactElement } from "react";
import { FaRegSmile, FaTicketAlt } from "react-icons/fa";
import { MdOutlineEventBusy } from "react-icons/md";
import { Styles } from "../styles/theme";

const styles: Styles = {
  wrapper: {
    bg: "primary.400",
    width: "100%",
    py: { md: 6 },
  },
  innerWrapper: {
    p: 6,
    maxWidth: 800,
  },
  featureWrapper: {
    bg: "white",
    p: 2,
    borderRadius: 8,
    alignItems: "center",
    textAlign: "center",
  },
  featureIcon: {
    w: 16,
    h: 16,
    alignItems: "center",
    justifyContent: "center",
    color: "white",
    rounded: "full",
    bg: "primary.500",
    mb: 1,
  },
  featureTitle: { fontWeight: 600 },
  featureText: {
    color: "gray.600",
  },
};

interface FeatureProps {
  title: string;
  text: string;
  icon: ReactElement;
}

const Feature = ({ title, text, icon }: FeatureProps) => {
  return (
    <Stack sx={styles.featureWrapper}>
      <Flex sx={styles.featureIcon}>{icon}</Flex>
      <Text sx={styles.featureTitle}>{title}</Text>
      <Text sx={styles.featureText}>{text}</Text>
    </Stack>
  );
};

export default function Features() {
  return (
    <Flex flexDir={"column"} alignItems={"center"} py={6}>
      <Heading
        fontWeight={600}
        fontSize={{ base: "2xl", sm: "2xl", md: "4xl" }}
        lineHeight={"110%"}
        my={6}
      >
        <Text as={"span"}>The </Text>
        <Text as={"span"} color={"primary.500"}>
          Flight Adhoc{" "}
        </Text>
        <Text as={"span"}>Advantage</Text>
      </Heading>
      <Center sx={styles.wrapper}>
        <Box sx={styles.innerWrapper}>
          <SimpleGrid columns={{ base: 1, md: 3 }} spacing={10}>
            <Feature
              icon={<Icon as={MdOutlineEventBusy} w={10} h={10} />}
              title={"NO MEMBERSHIP"}
              text={
                "You don’t need to pay a membership or joining fee to have access to Fly Adhocs on-demand, real-time and available aircraft instantly."
              }
            />
            <Feature
              icon={<Icon as={FaRegSmile} w={10} h={10} />}
              title={"TRANSPARENCY"}
              text={
                "Available for everyone to view, compare, and book private aircraft. Fly Adhoc bridges the gap between you and the operator of the aircraft direct."
              }
            />
            <Feature
              icon={<Icon as={FaTicketAlt} w={10} h={10} />}
              title={"SEARCH, BOOK + PAY DIRECT"}
              text={
                "Search for your flight in real-time, book now and pay directly to the aircraft operator."
              }
            />
          </SimpleGrid>
        </Box>
      </Center>
    </Flex>
  );
}
