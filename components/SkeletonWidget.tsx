import { Flex, Skeleton, Spacer } from "@chakra-ui/react";
import React from "react";
import { Styles } from "../styles/theme";

const styles: Styles = {
  wrapper: {
    flexDir: "column",
    p: 6,
    bg: "white",
    width: "100%",
    height: 430,
    maxW: 1240,
  },
  skeleton: {
    height: 32,
    width: "30%",
    alignSelf: "flex-end",
  },
};
export default function SkeletonWidget() {
  return (
    <Flex sx={styles.wrapper}>
      <Skeleton height="52px" width={"30%"} alignSelf={"flex-end"} />
      <Spacer />
      <Flex justify={"space-around"}>
        <Skeleton sx={styles.skeleton} />
        <Skeleton sx={styles.skeleton} />
        <Skeleton sx={styles.skeleton} />
      </Flex>
      <Spacer />
      <Flex justify={"space-around"}>
        <Skeleton sx={styles.skeleton} />
        <Skeleton sx={styles.skeleton} />
        <Skeleton sx={styles.skeleton} />
      </Flex>
      <Spacer />
    </Flex>
  );
}
