import { Box } from "@chakra-ui/layout";
import Image from "next/image";
import { FC } from "react";
import logo from "../public/logo.png";

const Logo: FC = () => {
  return (
    <Box sx={{ maxWidth: "480px" }}>
      <Image
        src={logo}
        alt="Private Jet Hero Image"
        // width={500} automatically provided
        // height={500} automatically provided
        // blurDataURL="data:..." automatically provided
        // placeholder="blur" // Optional blur-up while loading
      />
    </Box>
  );
};

export default Logo;
