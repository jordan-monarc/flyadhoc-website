import { Box, Flex, Heading, Text } from "@chakra-ui/layout";
import React, { FC } from "react";
import { Styles } from "../styles/theme";
import SkeletonWidget from "./SkeletonWidget";

const styles: Styles = {
  wrapper: {
    flexDir: "column",
    justifyContent: "center",
    alignItems: "center",
    py: 20,
    minHeight: 600,
  },
  widget: {
    maxW: { base: "100%", lg: 1240 },
  },
};

interface WidgetProps {
  loaded: boolean;
}

const Widget: FC<WidgetProps> = ({ loaded }) => {
  return (
    <Flex sx={styles.wrapper}>
      <Heading color={"black"} mb={5} fontSize={{ base: "3xl", md: "5xl" }}>
        <Text as={"span"}>Book </Text>
        <Text as={"span"} color={"primary.500"}>
          Now
        </Text>
      </Heading>
      <Box id={"monarc-global-widget"} sx={styles.widget} />
      {!loaded && <SkeletonWidget />}
    </Flex>
  );
};

export default Widget;
