import { Flex, Heading, Text } from "@chakra-ui/layout";
import { Box } from "@chakra-ui/react";
import Image from "next/image";
import firstImage from "../public/grid_image_01.jpg";
import secondImage from "../public/grid_image_02.jpg";
import thirdImage from "../public/grid_image_03.jpg";
import { Styles } from "../styles/theme";

const styles: Styles = {
  wrapper: {
    display: "flex",
    justifyContent: "center",
  },
  content: { maxWidth: 1140 },
  section: {
    flexWrap: { base: "wrap", md: "nowrap" },
  },
  sectionBox: {
    padding: 6,
  },
  image: { maxWidth: { base: 768, md: "50%" }, borderRadius: 5 },
  textWrapper: { display: "flex", flexDir: "column" },
  midTextWrapper: {
    order: { md: -1 },
  },
  heading: { mb: 5 },
  text: { color: "gray.600" },
};

export default function Grid() {
  return (
    <>
      <Flex justify={"center"}>
        <Heading
          fontWeight={600}
          fontSize={{ base: "2xl", sm: "2xl", md: "4xl" }}
          lineHeight={"110%"}
          my={6}
        >
          <Text as={"span"} color={"black"}>
            Why{" "}
          </Text>
          <Text as={"span"} color={"primary.400"}>
            Flight Adhoc?
          </Text>
        </Heading>
      </Flex>

      <Box sx={styles.wrapper}>
        <Box sx={styles.content}>
          <Flex sx={styles.section}>
            <Box sx={{ ...styles.image, ...styles.sectionBox }}>
              <Image src={firstImage} alt="Private Jet Image" />
            </Box>
            <Box sx={{ ...styles.textWrapper, ...styles.sectionBox }}>
              <Heading sx={styles.heading}>A Global First</Heading>
              <Text sx={styles.text}>
                Introducing FLY ADHOC, simply enter your flight details, and all
                available aircraft are produced live, in real-time, directly
                from the aircraft operators.
                <br />
                <br />
                With real-time availability, you can be assured that the
                aircraft you are booking is the aircraft you will receive.
              </Text>
            </Box>
          </Flex>
          <Flex sx={styles.section}>
            <Box sx={{ ...styles.image, ...styles.sectionBox }}>
              <Image src={secondImage} alt="Private Jet Image" />
            </Box>
            <Flex
              sx={{
                ...styles.textWrapper,
                ...styles.midTextWrapper,
                ...styles.sectionBox,
              }}
            >
              <Heading sx={styles.heading}>Search. Choose. Book.</Heading>
              <Text sx={styles.text}>
                Need to get somewhere in a hurry? Are you keen to avoid crowded
                airports and long check-ins?
                <br />
                <br />
                Aircraft charter is the only mode of air travel that allows you
                to meet every business, leisure, and family travel on your
                personal terms and now at your fingertips.
                <br />
                <br />
                Aircraft charter is easier than you think, and now with
                real-time pricing and instant booking capabilities
              </Text>
            </Flex>
          </Flex>
          <Flex sx={styles.section}>
            <Box sx={{ ...styles.image, ...styles.sectionBox }}>
              <Image src={thirdImage} alt="Private Jet Image" />
            </Box>
            <Box sx={{ ...styles.textWrapper, ...styles.sectionBox }}>
              <Heading sx={styles.heading}>Direct From The Source</Heading>
              <Text sx={styles.text}>
                Get direct access to aircraft and aircraft operators, small and
                large.
                <br />
                <br />
                Choose from a range of single – engine, turboprop, small to
                large executive jets, or even airliners; we have an aircraft for
                all your travel needs.
                <br />
                <br />
                Fly Adhoc can tailor to all needs and budgets, whether for one
                person, a family of six, a golf trip with the fellas, or hen’s
                night with the ladies.
              </Text>
            </Box>
          </Flex>
        </Box>
      </Box>
    </>
  );
}
